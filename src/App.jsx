import { useState } from 'react'
import './App.css'
import TransferList from './components/TransferList'

function App() {

  return (
    <>
      <TransferList />
    </>
  )
}

export default App
