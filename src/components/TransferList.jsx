import React from 'react'
import { useEffect, useState } from 'react';


export default function TransferList() {

    const [list, setList] = useState({
        "JS": [false, 'left'], 
        'HTML' : [false, 'left'],
        'CSS' : [false, 'left'],
        'TS' : [false, 'left'],
        'React': [false, 'right'],
        'Angular': [false, 'right'],
        'Vue': [false, 'right'],
        'Svelte': [false, 'right']
    });

    
    function handleSwitch(type){
        if(type == "all_left"){
            
            setList( prevState => {

                const updatedList = {}
                Object.entries(prevState).forEach((item)=> {
                    updatedList[item[0]] = [item[1][0], 'left']
                })

                return updatedList  
            })
        }
        else if(type=="all_right"){
            setList( prevState => {
                const updatedList = {}
                Object.entries(prevState).forEach((item)=> {
                    updatedList[item[0]] = [item[1][0], 'right']
                })

                return updatedList  
            })
        }
        else if(type=="left"){

            setList(prevState => {
                const updatedList = {}

                Object.entries(prevState).forEach((item)=> {
                    if(item[1][0] && item[1][1]=='right'){
                        updatedList[item[0]] = [item[1][0], 'left']
                    }else{
                        updatedList[item[0]] = item[1]
                    }
                })

                return updatedList
            })
        }else{

            setList(prevState => {
                const updatedList = {}

                Object.entries(prevState).forEach((item)=> {
                    if(item[1][0] && item[1][1]=='left'){
                        updatedList[item[0]] = [item[1][0], 'right']
                    }else{
                        updatedList[item[0]] = item[1]
                    }
                })

                return updatedList
            })
        }
    }

    function changeState(item){
        setList(prevState => ({
            ...prevState,
            [item]: [!prevState[item][0], prevState[item][1]]
        }))

    }




  return (
    <div className='container' style={{display:'flex'}}>
        <div className='left_container' style={{width: '50%', height: 'auto', border: '0.09rem solid black', display:'flex', flexDirection: 'column'}}>
            {
                list && Object.entries(list).map((item)=> {
                    return item[1][1]=='left' && <div style={{padding: '0.5rem 1rem'}} >

                        <input type="checkbox" value={`${item}`} className={`${item}`}  placeholder={`${item}`} checked={item[1][0]} onChange={()=>{changeState(item[0])}} />
                        <label > {item[0]}</label>
                    </div>
                })
            }
        </div>
        <div className='toggle_controller' style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-evenly', padding: '0 3rem', border: '0.09rem solid black'}}>
            <button onClick={()=> {handleSwitch("all_left") }}> {`<<`} </button>
            <button onClick={()=> {handleSwitch("left")}}> {`<`} </button>
            <button onClick={()=> {handleSwitch("all_right")}} > {`>>`} </button>
            <button onClick={()=> {handleSwitch("right")}}> {`>`} </button>
        </div>
        <div className='right_container' style={{width: '50%', height: 'auto', border: '0.09rem solid black'}}>
            {
                list && Object.entries(list).map((item)=> {
                    return item[1][1]=='right' && <div style={{padding: '0.5rem 1rem'}} >

                        <input type="checkbox" value={`${item}`} className={`${item}`}  placeholder={`${item}`} checked={item[1][0]} onChange={()=>{changeState(item[0])}} />
                        <label > {item[0]}</label>
                    </div>
                })
            }
        </div>
    </div>
  )
}
